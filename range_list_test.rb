require './range_list'

source_data = [
  { key: 1, action: 'add', data: [1,5], print_target: "[1, 5)" },
  { key: 2, action: 'add', data: [10,20], print_target: "[1, 5) [10, 20)" },
  { key: 3, action: 'add', data: [20,20], print_target: "[1, 5) [10, 20)" },
  { key: 4, action: 'add', data: [20,21], print_target: "[1, 5) [10, 21)" },
  { key: 5, action: 'add', data: [2,4], print_target: "[1, 5) [10, 21)" },
  { key: 6, action: 'add', data: [3,8], print_target: "[1, 8) [10, 21)" },
  { key: 7, action: 'remove', data: [10,10], print_target: "[1, 8) [10, 21)" },
  { key: 8, action: 'remove', data: [10,11], print_target: "[1, 8) [11, 21)" },
  { key: 9, action: 'remove', data: [15,17], print_target: "[1, 8) [11, 15) [17, 21)" },
  { key: 10, action: 'remove', data: [3,19], print_target: "[1, 3) [19, 21)" }
]

range = RangeList.new
source_data.each do |item|
  range.send(item[:action], item[:data])
  if range.print != item[:print_target]
    raise item
  end
end

puts "test ok"
