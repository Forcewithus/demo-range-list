class RangeList
  attr_accessor :list

  def initialize
    @list = []
  end

  def add(range)
    return false unless valid?(range)

    if list.empty?
      @list = range
      return true
    end

    left_index = get_index(range[0])
    right_index = get_index(range[1])

    # in the same range, no insert
    return true if left_index%2 == 1 && left_index == right_index

    @list.insert(left_index, range[0])
    @list.insert(right_index + 1, range[1])
    if right_index == left_index
      # in the same scope(not in range), inserted, no remove
      return true
    end

    # remove new range covered index
    remove_left_index = left_index%2 == 0 ? left_index : [0, left_index - 1].max
    remove_right_index = right_index%2 == 0 ? right_index + 1 : right_index + 2

    @list.slice!(remove_left_index + 1, remove_right_index - remove_left_index - 1)

    true
  end

  def remove(range)
    return false if list.empty?
    return false unless valid?(range)

    left_index = get_index(range[0])
    right_index = get_index(range[1])

    if left_index == right_index
      # in the same scope(out of range), no remove
      return true if left_index%2 == 0

      # in the same range
      @list.insert(right_index, *range)
      return true
    end

    remove_left_index = left_index
    remove_right_index = right_index%2 == 0 ? right_index : right_index - 1

    @list[remove_left_index..remove_right_index] = [nil] * (remove_right_index - remove_left_index + 1)

    # in some range
    if left_index%2 == 1
      @list[left_index] = range[0]
    end

    # in some range
    if right_index%2 == 1
      @list[right_index - 1] = range[1]
    end

    @list.compact!

    true
  end

  def valid?(range)
    range[0] < range[1]
  end

  # return target position index
  # index % 2 == 0 mean target not in any range, otherwise in some exists range
  def get_index(target)
    list.each_with_index do |n, i|
      if n >= target
        return i
      end
    end

    list.length
  end

  def print
    strs = []
    0.step(list.length - 1, 2) do |i|
      strs.push "[#{list[i]}, #{list[i+1]})"
    end
    strs.join(' ')
  end
end
